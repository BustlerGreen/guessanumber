﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guessAnumber
{
    /// <summary>
    /// intrface segregation
    /// </summary>
    public class GuessNumPlay
    {
        private INumGenerator _generator;
        private IEntryChecker _checker;
        private bool _numguesed;
        private int _tries;

        public GuessNumPlay(INumGenerator generator, IEntryChecker checker)
        {
            _generator = generator;
            _checker = checker;
        }

        public void Start(ExtSetting settings)
        {
            //liskov substitutus - call method with derived from expected class ExtSetting instead of Setting
            
            _generator.SetBounds(settings);
            //ocp doesn't need to be modified if checker or generator was changed
            _checker.SetOrigin(_generator.Generate());
            _numguesed = false;
            _tries = settings.TryCount;
        }

        public bool CheckEntry(int entry)
        {
            if (_numguesed) { Console.WriteLine("game finished. You won"); return true; }
            if(0 >= _tries) { Console.WriteLine("No more tires. Try again"); return true; }
            string mess = string.Empty;
            
            var res = _checker.CheckNum(entry, out mess);
            Console.WriteLine(mess);
            _tries--;
            return _numguesed = res;
        }
    }
}
