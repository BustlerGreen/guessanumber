﻿// See https://aka.ms/new-console-template for more information

using guessAnumber;


Console.WriteLine("'q' for exit, 's' for start, num for guess");
var readChar = Console.ReadLine();
int status = 0;
var gen = new NumGen();
var ch = new NumChecker();
var mash = new GuessNumPlay(gen,ch);
var es = new ExtSetting() { HighEdge = 10, LowEdge = 1, TryCount = 5 };
while (readChar != "q")
{
    
    if (1 == status) 
    {
        try
        {
            var entry = Convert.ToInt32(readChar);
            var res = mash.CheckEntry(entry);
            if (res) status = 0;
        }
        catch (Exception e)
        {
            Console.WriteLine("etered not a number. try again");
        }
    }
    else if("s" == readChar)
    {
        mash.Start(es);
        status = 1;
        Console.WriteLine("game started. enter a number");
    }
    else Console.WriteLine("'q' for exit, 's' for start, num for guess");
    readChar = Console.ReadLine();

}


