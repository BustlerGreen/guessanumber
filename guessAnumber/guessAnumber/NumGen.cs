﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace guessAnumber
{
    //initial condition from settings
    public class Settings
    {
        public int LowEdge { get; set; } = 0;
        public int HighEdge { get; set; } = 10;
    }

    public class ExtSetting : Settings
    {
        public int TryCount { get; set; } = 3;
    }

    //single responsibility and dependency inversion

    public interface INumGenerator
    {
        public void SetBounds(Settings sett);
        public int Generate();
        public int LowEdge { get; }
        public int HighEdge { get; }
        public int Value { get; }
    }
    
    
    public class NumGen : INumGenerator
    {
        protected int _lowEdge;
        protected int _highEdge;
        protected int  _value = 0;
        protected Random _random;
        public int LowEdge { get => _lowEdge; }
        public int HighEdge { get => _highEdge; }
        public int Value { get => _value; }

        public NumGen()
        {
            _random = new Random();
            _lowEdge = 0;
            _highEdge = 1;
        }

        public void SetBounds(Settings sett)
        {
            _lowEdge = sett.LowEdge;
            _highEdge = sett.HighEdge;   
        }

        public int Generate()
        {
            return _value = (int)_random.NextInt64(_lowEdge, _highEdge);
        }

        
    }
}
