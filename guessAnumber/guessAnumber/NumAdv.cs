﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace guessAnumber
{
    //single responsibility and dependency inversion
    public interface IEntryChecker
    {
        public void SetOrigin(int origin);
        public bool CheckNum(int entry, out string mess);
    }
    public class NumChecker : IEntryChecker
    {
        protected int _origin = -1;
        public void SetOrigin(int origin)
        {
            _origin = origin;
        }
        public bool CheckNum(int entry, out string mess)
        {
            mess = string.Empty;
            if (entry > _origin) {mess =  "entered num is greater than origin"; return false;}
            else if (entry < _origin) {mess =  "entered num is lower than origin"; return false; }
            mess =  "You successfully guessed a number"; return true;
        }


    }

    
}
